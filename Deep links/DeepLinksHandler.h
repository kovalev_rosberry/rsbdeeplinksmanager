//
//  DeepLinksHandler.h
//  Deep links
//
//  Created by Anton Kovalev on 05.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSBDeepLinksManager.h"

@interface DeepLinksHandler : NSObject <RSBDeepLinkHandler>

@end
