//
//  TRDDeepLink.h
//  Collaborate
//
//  Created by Anton Kovalev on 30.03.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, RSBDeepLinkType) {
    RSBDeepLinkTypeUnknown,
    RSBDeepLinkTypeURLScheme,
    RSBDeepLinkTypeUniversalLink
};

NS_ASSUME_NONNULL_BEGIN

@interface RSBDeepLink : NSObject

- (instancetype)initWithURL:(NSURL *)url;
- (instancetype)initWithUserActivity:(NSUserActivity *)userActivity;

@property (nonatomic, readonly) NSURL *url;
@property (nonatomic, nullable, readonly) NSUserActivity *userActivity;
@property (nonatomic, readonly) RSBDeepLinkType type;

@end

NS_ASSUME_NONNULL_END
