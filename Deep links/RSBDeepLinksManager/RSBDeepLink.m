//
//  TRDDeepLink.m
//  Collaborate
//
//  Created by Anton Kovalev on 30.03.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "RSBDeepLink.h"

@interface RSBDeepLink ()

@property (nonatomic, readwrite) NSURL *url;
@property (nonatomic, readwrite) NSUserActivity *userActivity;
@property (nonatomic, readwrite) RSBDeepLinkType type;

@end

@implementation RSBDeepLink

- (instancetype)initWithURL:(NSURL *)url {
    self = [super init];
    if (self) {
        self.url = url;
        self.type = RSBDeepLinkTypeURLScheme;
    }
    return self;
}

- (instancetype)initWithUserActivity:(NSUserActivity *)userActivity {
    self = [super init];
    if (self) {
        self.url = userActivity.webpageURL;
        self.userActivity = userActivity;
        self.type = RSBDeepLinkTypeUniversalLink;
    }
    return self;
}

@end
