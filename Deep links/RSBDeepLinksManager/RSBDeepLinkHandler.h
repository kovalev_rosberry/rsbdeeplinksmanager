//
//  RSBDeepLinkRouter.h
//  Deep links
//
//  Created by Anton Kovalev on 05.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

@protocol RSBDeepLinkHandler <NSObject>

- (BOOL)handleURL:(nonnull NSURL *)url;

@end
