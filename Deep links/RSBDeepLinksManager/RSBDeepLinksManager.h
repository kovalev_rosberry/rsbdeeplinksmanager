//
//  TRDDeepLinksManager.h
//  Collaborate
//
//  Created by Anton Kovalev on 30.03.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSBDeepLink.h"
#import "RSBDeepLinkHandler.h"

NS_ASSUME_NONNULL_BEGIN

@interface RSBDeepLinksManager : NSObject

@property (nonatomic) BOOL isReadyToHandleLinks;

@property (nonatomic, nullable) id<RSBDeepLinkHandler> handler;

+ (instancetype)instance;
- (instancetype)initWithHosts:(NSArray<NSString *> *)hosts NS_DESIGNATED_INITIALIZER;

- (BOOL)handleDeepLink:(RSBDeepLink *)link;
- (void)checkStartFromLink:(NSDictionary *)launchOptions;
- (void)ignoreDelayedDeepLink;

@end

NS_ASSUME_NONNULL_END
