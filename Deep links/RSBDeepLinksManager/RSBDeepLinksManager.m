//
//  TRDDeepLinksManager.m
//  Collaborate
//
//  Created by Anton Kovalev on 30.03.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "RSBDeepLinksManager.h"

@interface RSBDeepLinksManager ()

@property (nonatomic) RSBDeepLink *delayedLink;
@property (nonatomic) NSArray *hosts;

@end

@implementation RSBDeepLinksManager

static RSBDeepLinksManager *instance;

+ (instancetype)instance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!instance) {
            instance = [[RSBDeepLinksManager alloc] init];
        }
    });
    return instance;
}

- (instancetype)init {
    self = [self initWithHosts:@[]];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithHosts:(nonnull NSArray<NSString *> *)hosts {
    self = [super init];
    
    if (self) {
        NSAssert(hosts.count > 0, @"Use initWithHosts: method for creating links manager");
        NSMutableArray *ma = [NSMutableArray array];
        for (NSString *host in hosts) {
            [ma addObject:[self stringByRemovingHTTPWWWSymbolsFromString:host]];
        }
        self.hosts = [NSArray arrayWithArray:ma];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
    }
    
    @synchronized (self) {
        if (!instance) {
            instance = self;
        }
    }
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Notifications

- (void)applicationWillResignActive {
    self.delayedLink = nil;
}

#pragma mark - Public

- (BOOL)handleDeepLink:(RSBDeepLink *)link {
    if (self.isReadyToHandleLinks) {
        NSURL *url = link.url;
        if (link.type == RSBDeepLinkTypeURLScheme) {
            if ([url.scheme isEqualToString:@"trackd"]) {
                if ([self hostStringIsValid:url.host]) {
                    return [self handleURL:url];
                }
            }
        } else if (link.type == RSBDeepLinkTypeUniversalLink) {
            if ([self hostStringIsValid:url.host]) {
                return [self handleURL:url];
            }
        } else {
            NSString *description = NSLocalizedString(@"Unknown type of the links unsupported", nil);
            [[NSException exceptionWithName:@"TRDDeepLinksManager exception" reason:description userInfo:@{NSLocalizedDescriptionKey:description}] raise];
        }
    } else {
        self.delayedLink = link;
        return YES;
    }
    return NO;
}

- (void)ignoreDelayedDeepLink {
    self.delayedLink = nil;
}

- (BOOL)handleURL:(NSURL *)url {
    if (!self.handler) {
        [NSException raise:@"RSBDeepLinksManager exception" format:@"You must setup handler property which conforms to %@ protocol", @protocol(RSBDeepLinkHandler)];
    }
    return [self.handler handleURL:url];
}

- (void)checkStartFromLink:(NSDictionary *)launchOptions {
    NSURL *url = launchOptions[UIApplicationLaunchOptionsURLKey];
    
    RSBDeepLink *link = nil;
    if (url) {
        link = [[RSBDeepLink alloc] initWithURL:url];
    }
    
    if (link) {
        [self handleDeepLink:link];
    }
}

#pragma mark - Setters

- (void)setIsReadyToHandleLinks:(BOOL)isReadyToHandleLinks {
    _isReadyToHandleLinks = isReadyToHandleLinks;
    
    if (self.isReadyToHandleLinks) {
        if (self.delayedLink) {
            [self handleDeepLink:self.delayedLink];
            self.delayedLink = nil;
        }
    } else {
        self.handler = nil;
        self.delayedLink = nil;
    }
}

#pragma mark - Helper methods

- (NSString *)stringByRemovingHTTPWWWSymbolsFromString:(NSString *)string {
    NSString *rString = [[string lowercaseString] stringByReplacingOccurrencesOfString:@"http://"
                                                                            withString:@""
                                                                               options:NSAnchoredSearch
                                                                                 range:NSMakeRange(0, [string length])];
    rString = [[rString lowercaseString] stringByReplacingOccurrencesOfString:@"https://"
                                                                   withString:@""
                                                                      options:NSAnchoredSearch
                                                                        range:NSMakeRange(0, [rString length])];
    rString = [[rString lowercaseString] stringByReplacingOccurrencesOfString:@"www."
                                                                   withString:@""
                                                                      options:NSAnchoredSearch
                                                                        range:NSMakeRange(0, [rString length])];
    return rString;
}

- (BOOL)hostStringIsValid:(NSString *)host {
    BOOL hostIsValid = NO;
    
    NSString *hostWithoutHTTPWWWSymbols = [self stringByRemovingHTTPWWWSymbolsFromString:host];
    for (NSString *h in self.hosts) {
        if ([h isEqualToString:hostWithoutHTTPWWWSymbols]) {
            hostIsValid = YES;
            break;
        }
    }
    
    return hostIsValid;
}

@end
