Pod::Spec.new do |s|

  s.name           = "RSBDeepLinksManager"
  s.version        = "0.0.1"
  s.summary        = "Work with open in app links easy"

  s.description   = <<-DESC
                    We often face with task like handling link to open some specific screen in the app
                    and now it's easy as pie. You just should set valid hosts and handle URLs as you need.
                    DESC

  s.homepage      = "https://bitbucket.org/kovalev_rosberry/rsbdeeplinksmanager.git"
  
  s.license       = { :type => "MIT", :file => "LICENSE" }
  s.author        = { "Anton Kovalev" => "anton.kovalev@rosberry.com" }

  s.platform      = :ios, "7.0"

  s.source        = { :git => "https://bitbucket.org/kovalev_rosberry/rsbdeeplinksmanager.git", :tag => "0.0.1" }

  s.source_files  = "Classes", "Deep links/RSBDeepLinksManager/**/*.{h,m}"
  s.exclude_files = "Classes/Exclude"

end
